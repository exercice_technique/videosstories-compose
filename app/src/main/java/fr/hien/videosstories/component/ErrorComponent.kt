package fr.hien.videosstories.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import fr.hien.videosstories.R
import fr.hien.videosstories.annotation.CombinedPreviews
import fr.hien.videosstories.ui.model.ErrorUIModel
import fr.hien.videosstories.ui.model.TextUIModel

@Composable
fun ErrorComponent(
    modifier: Modifier = Modifier,
    uiModel: ErrorUIModel,
    onButtonClick: () -> Unit
) {
    Column(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = uiModel.text.asString())
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = onButtonClick,
            colors = ButtonDefaults.textButtonColors(
                contentColor = Color.White,
                containerColor = Color.Blue
            )
        ) {
            Text(stringResource(id = R.string.retry))
        }
    }
}

@Composable
@CombinedPreviews
fun PreviewErrorComponent() {
    ErrorComponent(
        uiModel = ErrorUIModel(
            text = TextUIModel.DynamicString("Error of get data")
        ),
        onButtonClick = {}
    )
}