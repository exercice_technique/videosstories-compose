package fr.hien.videosstories

import android.content.Context
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.media3.common.MediaItem
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory
import fr.hien.videosstories.ui.model.VideoUIModel
import fr.hien.videosstories.ui.state.FootballUIState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

@androidx.annotation.OptIn(androidx.media3.common.util.UnstableApi::class)
class MainViewModel(
    private val playerManager: PlayerManager,
    private val defaultMediaSourceFactory: DefaultMediaSourceFactory
) : ViewModel() {

    private val _uiState = MutableStateFlow<FootballUIState>(FootballUIState.Loading)
    val uiState: StateFlow<FootballUIState> = _uiState.asStateFlow()

    fun play(
        context: Context
    ) {
        viewModelScope.launch {
            _uiState.update { FootballUIState.Loading }
            val videoPlayer = playerManager.getPlayer(
                context = context
            )
            Uri.parse("https://vod-eurosport.akamaized.net/ebu-au/2019/12/08/snookfinish-1269077-700-512-288.mp4")?.let {
                val mediaSource = defaultMediaSourceFactory.createMediaSource(
                    MediaItem.fromUri(it)
                )
                videoPlayer.setMediaSource(mediaSource)
                videoPlayer.prepare()
                _uiState.update {
                    FootballUIState.Success(
                        uiModel = VideoUIModel(videoPlayer = videoPlayer)
                    )
                }
            }
        }
    }
}