package fr.hien.videosstories.ui.model

import androidx.compose.runtime.Immutable
import androidx.media3.common.Player

@Immutable
data class VideoUIModel(
    val videoPlayer: Player?
)