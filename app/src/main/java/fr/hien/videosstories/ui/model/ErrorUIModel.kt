package fr.hien.videosstories.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class ErrorUIModel(
    val text: TextUIModel
)
