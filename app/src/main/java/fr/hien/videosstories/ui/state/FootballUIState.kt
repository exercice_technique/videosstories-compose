package fr.hien.videosstories.ui.state

import androidx.compose.runtime.Immutable
import fr.hien.videosstories.ui.model.ErrorUIModel
import fr.hien.videosstories.ui.model.TextUIModel
import fr.hien.videosstories.ui.model.VideoUIModel

sealed class FootballUIState {
    object Loading : FootballUIState()

    @Immutable
    data class Error(
        val uiModel: ErrorUIModel
    ) : FootballUIState()

    @Immutable
    data class Success(
        val uiModel: VideoUIModel
    ) : FootballUIState()
}