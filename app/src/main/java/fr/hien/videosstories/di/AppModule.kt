package fr.hien.videosstories.di

import android.content.Context
import androidx.media3.database.StandaloneDatabaseProvider
import androidx.media3.datasource.DataSource
import androidx.media3.datasource.DefaultDataSource
import androidx.media3.datasource.DefaultHttpDataSource
import androidx.media3.datasource.FileDataSource
import androidx.media3.datasource.cache.CacheDataSink
import androidx.media3.datasource.cache.CacheDataSource
import androidx.media3.datasource.cache.NoOpCacheEvictor
import androidx.media3.datasource.cache.SimpleCache
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory
import fr.hien.videosstories.MainViewModel
import fr.hien.videosstories.PlayerManager
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module
import java.io.File

val appModule = module {
    single { Dispatchers.IO }
    single { PlayerManager() }
    single {
        provideVideoContentDirectory(get())
    }
    single {
        provideVideoDownloadCache(get(), get())
    }
    single {
        provideCacheDataSourceFactory(get(), get())
    }
    single {
        provideDefaultMediaSourceFactory(get())
    }
    viewModelOf(::MainViewModel)
}

@androidx.annotation.OptIn(androidx.media3.common.util.UnstableApi::class)
fun provideDefaultMediaSourceFactory(
    cacheDataSourceFactory: DataSource.Factory
): DefaultMediaSourceFactory {
    return DefaultMediaSourceFactory(cacheDataSourceFactory)
}

@androidx.annotation.OptIn(androidx.media3.common.util.UnstableApi::class)
fun provideCacheDataSourceFactory(
    videoCache: SimpleCache,
    context: Context
): DataSource.Factory {
    val cacheSink = CacheDataSink.Factory().setCache(videoCache)
    val upstreamFactory = DefaultDataSource.Factory(
        context,
        DefaultHttpDataSource.Factory()
    )
    return CacheDataSource.Factory()
        .setCache(videoCache)
        .setCacheWriteDataSinkFactory(cacheSink)
        .setCacheReadDataSourceFactory(
            FileDataSource.Factory()
        )
        .setUpstreamDataSourceFactory(upstreamFactory)
        .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)
}

@androidx.annotation.OptIn(androidx.media3.common.util.UnstableApi::class)
fun provideVideoDownloadCache(
    contentDirectory: File,
    context: Context
): SimpleCache {
    return SimpleCache(
        contentDirectory,
        NoOpCacheEvictor(),
        StandaloneDatabaseProvider(context)
    )
}

fun provideVideoContentDirectory(
    context: Context
): File {
    return File(
        context.getExternalFilesDir(null),
        "video_cache"
    )
}
