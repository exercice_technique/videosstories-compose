package fr.hien.videosstories

import android.content.Context
import androidx.media3.common.PlaybackException
import androidx.media3.common.Player
import androidx.media3.exoplayer.ExoPlayer

class PlayerManager {

    fun getPlayer(
        context: Context,
        onError: () -> Unit = {},
        onLoading: (isLoading: Boolean) -> Unit = {},
        onPlaying: (player: ExoPlayer) -> Unit = {}
    ): ExoPlayer {
        return ExoPlayer.Builder(context).build().apply {
            addListener(
                object : Player.Listener {
                    private var isOnPlayerError = false
                    override fun onPlayerError(error: PlaybackException) {
                        super.onPlayerError(error)
                        isOnPlayerError = true
                        onError()
                    }

                    override fun onIsLoadingChanged(isLoading: Boolean) {
                        super.onIsLoadingChanged(isLoading)
                        if (isOnPlayerError) return
                        onLoading(isLoading)
                    }

                    override fun onIsPlayingChanged(isPlaying: Boolean) {
                        super.onIsPlayingChanged(isPlaying)
                        if (isPlaying) {
                            // La vidéo est en cours de lecture, rien à faire
                            return
                        }
                        onPlaying(this@apply)
                    }
                }
            )
            //volume = 0f
            playWhenReady = true
            repeatMode = Player.REPEAT_MODE_ONE
        }
    }
}