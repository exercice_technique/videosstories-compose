package fr.hien.videosstories

import SportVideoComponent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import fr.hien.videosstories.component.ErrorComponent
import fr.hien.videosstories.component.LoaderComponent
import fr.hien.videosstories.ui.state.FootballUIState
import fr.hien.videosstories.ui.theme.VideosStoriesTheme
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.play(this)
        setContent {
            VideosStoriesTheme {
                when (val uiState = viewModel.uiState.collectAsStateWithLifecycle().value) {
                    FootballUIState.Loading -> LoaderComponent()
                    is FootballUIState.Error -> ErrorComponent(
                        uiModel = uiState.uiModel,
                        onButtonClick = { }
                    )
                    is FootballUIState.Success -> Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                        SportVideoComponent(
                            uiModel = uiState.uiModel,
                            onClick = {}
                        )
                    }
                }
            }
        }
    }
}